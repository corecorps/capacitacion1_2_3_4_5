﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Unir.ErpAcademico.DomainModules.Capacitacion.Aggregates.CustomValidationsAttributes
{
	public class YearValidationAttribute : ValidationAttribute
	{
		public override bool IsValid(object value)
		{
			//DateTime fechaAnio = new DateTime(1880, 1, 1);
			DateTime fechaAnio = Convert.ToDateTime(value);

			return fechaAnio.Year >= 1900;

		}
	}
}



using ApiModel.Poco;
using ApiRepositories.Parameters;
using BussinessLogic.Implementations;
using BussinessLogic.Interfaces;

namespace ApiCore {
    using System;
    using System.Collections.Generic;


    using ApiCore.Authentication;
    using ApiDataAccess;
    using ApiUnitWork;
    using Microsoft.AspNetCore.Authentication.JwtBearer;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.Hosting;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.IdentityModel.Logging;

    public class Startup {
        public Startup (IConfiguration configuration) {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices (IServiceCollection services)
        {           
            services.AddSingleton<IUnitOfWork> (option => new UnitOfWork (
                Configuration.GetConnectionString ("localOracle")
            ));

            services.Configure<MyConfiguration>(Configuration.GetSection("myConfiguration"));
            var tokenProvider = new JwtProvider ("issuer", "audience", "profexorrr_20000");
            services.AddSingleton<ITokenProvider> (tokenProvider);
            services.AddAuthentication (JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer (options => {
                    options.RequireHttpsMetadata = false;
                    options.TokenValidationParameters = tokenProvider.GetValidationParameters ();
                });
        

            services.AddAuthorization (auth => {
                auth.DefaultPolicy = new AuthorizationPolicyBuilder ()
                    .AddAuthenticationSchemes (JwtBearerDefaults.AuthenticationScheme)
                    .RequireAuthenticatedUser ()
                    .Build ();
            });
            services
            services.AddMvc().SetCompatibilityVersion (CompatibilityVersion.Version_3_0);
            services.AddMvc(option => option.EnableEndpointRouting = false);
            services.AddCors(opt =>
            {
                opt.AddPolicy("TestPolicy", builder =>
                {
                    builder.WithOrigins("http://localhost:9207")
                    .AllowAnyHeaders()
                    .AllowAnyMethod();
                });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure (IApplicationBuilder app, IWebHostEnvironment env) {
            if (env.IsDevelopment()) {
                app.UseDeveloperExceptionPage ();
                IdentityModelEventSource.ShowPII = true;
            } else {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            
            app.UseCors("TestPolicy");
            

        }
    }
}
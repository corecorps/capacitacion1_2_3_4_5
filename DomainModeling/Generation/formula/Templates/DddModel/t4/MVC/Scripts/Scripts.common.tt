﻿<#+
void WriteGenericListColumns(DisplaySet displaySet, Entity entity, bool addCheckBox, string checkBoxId, bool addAlerts, string gridStateObjectName, string excludeReference = "")
{	
	if(displaySet == null) return;
	
	int counter = 0;
	bool addVisible = !string.IsNullOrEmpty(gridStateObjectName);

	if(addCheckBox)
	{
#>
			{
                sTitle: '<input type="checkbox" id="chk-<#= checkBoxId #>-all" />',
                sWidth: "30px",
                bSortable: false
            },
<#+
		counter++;
	}
	
	foreach(var item in displaySet.Items)
	{
		if(!item.IsVisible()) continue;
	
		var scalar = entity.PresentationHierarchicalScalars
						   .FirstOrDefault(s => s.Name.Equals(item.Expression));
		
		var navigation = entity.PresentationHierarchicalNavigationProperties
							   .FirstOrDefault(s => !s.IsMultiple && s.RoleName.Equals(item.Expression));
		
		if(scalar == null && navigation == null) continue;
		
		if(navigation != null && navigation.RoleName.Equals(excludeReference)) continue;
		
		var expr = scalar != null ? scalar.Name : navigation.RoleName;
		
		if(addVisible)
		{
#>
			{
                sTitle: Globalize.formatMessage('Column<#= expr #>'),
                bSortable: <#= item.IsSortable() ? "true" : "false" #>,
                sWidth: "<#= item.Width() #>",
				bVisible: isNull(pageData.<#= gridStateObjectName #>.HiddenColumns) ? true : $.inArray(<#= counter #>, pageData.<#= gridStateObjectName #>.HiddenColumns) < 0
            },
<#+
		}
		else
		{
#>
			{
                sTitle: Globalize.formatMessage('Column<#= expr #>'),
                bSortable: <#= item.IsSortable() ? "true" : "false" #>,
                sWidth: "<#= item.Width() #>"
            },
<#+
		}
		
		counter++;
	}
	
	if(addAlerts)
	{
#>
			//{
            //    sTitle: Globalize.formatMessage('ColumnAlerts'),
            //    sWidth: "60px",
            //    bSortable: true
            //}, 
<#+
	}
#>
			{
                sTitle: Globalize.formatMessage('ColumnActions'),
                sWidth: "60px",
                bSortable: false
            }
<#+
}

const int TABLE_PARAMS_DEFAULT = 1;
const int TABLE_PARAMS_ORDINAL_FIRST = 2;

void WriteTableParams(DisplaySet displaySet, Entity entity, int tableParamsFormat, bool countCheckbox, string excludeReference = "")
{	
	int increment = countCheckbox ? 1 : 0;
	int counter = 0;

	foreach(var item in displaySet.Items)
	{
		if(!item.IsVisible()) continue;
		
		var scalar = entity.PresentationHierarchicalScalars
						   .FirstOrDefault(s => s.Name.Equals(item.Expression));
		
		var navigation = entity.PresentationHierarchicalNavigationProperties
							   .FirstOrDefault(np => np.RoleName.Equals(item.Expression));
		
		if(scalar == null && navigation == null) continue;
		
		if(navigation != null && navigation.RoleName.Equals(excludeReference)) continue;
		
		var expr = scalar != null ? scalar.Name : navigation.RoleName;
		
		if(tableParamsFormat == TABLE_PARAMS_ORDINAL_FIRST)
		{
#>
		  '<#= Hyphenate(expr) #>', <#= counter + increment #><#= displaySet.Items.Last() == item ? "" : "," #>
<#+
		}
		else
		{
#>
		  <#= counter + increment #>, '<#= Hyphenate(expr) #>'<#= displaySet.Items.Last() == item ? "" : "," #>
<#+
		}
		
		counter++;
	}
}

void WriteTableRows(DisplaySet displaySet, Entity entity, string excludeReference = "")
{	
	foreach(var item in displaySet.Items)
	{
		if(!item.IsVisible()) continue;
		
		var scalar = entity.PresentationHierarchicalScalars
						   .FirstOrDefault(s => s.Name.Equals(item.Expression));
		var navigation = entity.PresentationHierarchicalNavigationProperties
							   .FirstOrDefault(nav => nav.RoleName.Equals(item.Expression));
		
		if(scalar != null)
		{
			WriteTableRowScalarItem(item, scalar, entity);
		}
		if(navigation != null && !navigation.RoleName.Equals(excludeReference))
		{
			var prefix = "";
			
			if(Script.Entity.HasParent && Script.Entity.InheritanceStrategy == InheritanceStrategy.Simple)
			{
				var parentNav = Script.Entity.Parent.HierarchicalNavigationProperties.FirstOrDefault(s => s.RoleName.Equals(navigation.RoleName));
				
				if(parentNav != null)
				{
					prefix += Script.Entity.Parent.Name + ".";
				}
				
				if(Script.Entity.Parent.HasParent && Script.Entity.Parent.InheritanceStrategy == InheritanceStrategy.Simple)
				{
					var grandpaNav = Script.Entity.Parent.Parent.HierarchicalNavigationProperties.FirstOrDefault(s => s.Name.Equals(navigation.RoleName));
				
					if(grandpaNav != null)
					{
						prefix += Script.Entity.Parent.Name + "." + Script.Entity.Parent.Parent.Name + ".";
					}
				}
			}
			
			if(navigation.IsMandatory || (navigation.Entity is ValueObject))
			{
#>
    row.push('<span title="' + rowData.<#= prefix #><#= navigation.RoleName #>.DisplayName + '">' + summary(rowData.<#= prefix #><#= navigation.RoleName #>.DisplayName, <#= item.Length() #>, '...') + '</span>');
<#+
			}
			else
			{
#>
    row.push('<span title="' + (rowData.<#= prefix #>Has<#= navigation.RoleName #> ? rowData.<#= prefix #><#= navigation.RoleName #>.DisplayName : '') + '">' + summary((rowData.<#= prefix #>Has<#= navigation.RoleName #> ? rowData.<#= prefix #><#= navigation.RoleName #>.DisplayName : ''), <#= item.Length() #>, '...') + '</span>');
<#+
			}
		}
	}
}

void WriteTableRowScalarItem(DisplaySetItem item, ScalarProperty scalar, Entity entity)
{
	var prefix = "";
		
	if(Script.Entity.HasParent && Script.Entity.InheritanceStrategy == InheritanceStrategy.Simple)
	{
		var parentScalar = Script.Entity.Parent.HierarchicalScalars.FirstOrDefault(s => s.Name.Equals(scalar.Name));
		
		if(parentScalar != null)
		{
			prefix = Script.Entity.Parent.Name + ".";
		}
		
		if(Script.Entity.Parent.HasParent && Script.Entity.Parent.InheritanceStrategy == InheritanceStrategy.Simple)
		{
			var grandpaScalar = Script.Entity.Parent.Parent.HierarchicalScalars.FirstOrDefault(s => s.Name.Equals(scalar.Name));
		
			if(grandpaScalar != null)
			{
				prefix = Script.Entity.Parent.Name + "." + Script.Entity.Parent.Parent.Name + ".";
			}
		}
	}

	if(new[] { "datetime", "date" }.Contains(scalar.Type))
	{
		WriteTableRowDateTimeScalarItem(item, scalar, prefix);
	}
	else if (new[] { "time" }.Contains(scalar.Type))
	{
		WriteTableRowTimeScalarItem(item, scalar, prefix);
	}
	else if(scalar.IsFileType)
	{
		if("image".Equals(scalar.Type))
		{
			WriteTableRowImageScalarItem(item, scalar, prefix);
		}
		else
		{
			WriteTableRowFileScalarItem(item, scalar, entity, prefix);
		}
	}
	else if(scalar.IsNumericType)
	{
		WriteTableRowNumericScalarItem(item, scalar, prefix);
	}
	else if("bool".Equals(scalar.Type))
	{
		WriteTableRowBooleanScalarItem(item, scalar, prefix);
	}
	else
	{
		WriteTableRowDefaultScalarItem(item, scalar, prefix);
	}
}

void WriteTableRowDateTimeScalarItem(DisplaySetItem item, ScalarProperty scalar, string prefix = "")
{
	if(scalar.IsMandatory)
	{
#>
	row.push(Globalize.formatDateUsingMask(Globalize.parseDateISOString(rowData.<#= prefix #><#= scalar.Name #>)));
<#+
	}
	else
	{
#>
    if (rowData.<#= prefix #><#= scalar.Name #>) {
		row.push(Globalize.formatDateUsingMask(Globalize.parseDateISOString(rowData.<#= prefix #><#= scalar.Name #>)));
    } else {
        row.push('');
    }
<#+
	}
}

void WriteTableRowTimeScalarItem(DisplaySetItem item, ScalarProperty scalar, string prefix = "")
{
	if(scalar.IsMandatory)
	{
#>
	row.push(Globalize.formatDateUsingMask(Globalize.parseDateISOString('1970-01-01T' + rowData.<#= prefix #><#= scalar.Name #>), 'HH:mm:ss'));
<#+
	}
	else
	{
#>
	if(rowData.<#= prefix #><#= scalar.Name #>) {
	    row.push(Globalize.formatDateUsingMask(Globalize.parseDateISOString('1970-01-01T' + rowData.<#= prefix #><#= scalar.Name #>), 'HH:mm:ss'));
	} else {
		row.push('');
	}
<#+
	}
}

void WriteTableRowNumericScalarItem(DisplaySetItem item, ScalarProperty scalar, string prefix = "")
{
	var decimals = new [] { "decimal", "currency", "double", "percentage" };
	
	string localizedSuffix = decimals.Contains(scalar.Type) ? ".asLocalizedDecimal()" : "";

	if(scalar.IsMandatory)
	{
#>
	row.push(rowData.<#= prefix #><#= scalar.Name #><#= localizedSuffix #>);
<#+
	}
	else
	{
#>
	if(rowData.<#= scalar.Name #> != null) {
		row.push(rowData.<#= prefix #><#= scalar.Name #><#= localizedSuffix #>);
	} else {
		row.push('');
	}
<#+
	}
}

void WriteTableRowBooleanScalarItem(DisplaySetItem item, ScalarProperty scalar, string prefix = "")
{
	if(scalar.IsMandatory)
	{
#>
	row.push(rowData.<#= prefix #><#= scalar.Name #> == true ? Globalize.formatMessage('TextYes') : Globalize.formatMessage('TextNo'));
<#+
	}
	else
	{
#>
	if(rowData.<#= scalar.Name #> != null) {
		row.push(rowData.<#= prefix #><#= scalar.Name #> == true ? Globalize.formatMessage('TextYes') : Globalize.formatMessage('TextNo'));
	} else {
		row.push('');
	}
<#+
	}
}

void WriteTableRowDefaultScalarItem(DisplaySetItem item, ScalarProperty scalar, string prefix = "")
{
	var rowData = !UseTelerikEditor(scalar) ? "rowData." + prefix + scalar.Name : "stripTags(rowData." + prefix + scalar.Name + ")";

	if(scalar.IsMandatory)
	{
#>
	row.push('<span title="' + rowData.<#= prefix #><#= scalar.Name #> + '">' + summary(<#= rowData #>, <#= item.Length() #>, '...') + '</span>');
<#+
	}
	else
	{
#>
	if(rowData.<#= prefix #><#= scalar.Name #> != null) {
		row.push('<span title="' + rowData.<#= prefix #><#= scalar.Name #> + '">' + summary(<#= rowData #>, <#= item.Length() #>, '...') + '</span>');
	} else {
		row.push('');
	}
<#+
	}
}

void WriteTableRowImageScalarItem(DisplaySetItem item, ScalarProperty scalar, string prefix = "")
{
	if(scalar.IsMandatory)
	{
#>
    row.push('<a href="#" class="btn-show-<#= Hyphenate(scalar.Name) #>-thumb">' + Globalize.formatMessage("TextShow") + '</a>');
<#+
	}
	else
	{
#>
	if(rowData.<#= scalar.Name #> != null) {
		row.push('<a href="#" class="btn-show-<#= Hyphenate(scalar.Name) #>-thumb">' + Globalize.formatMessage("TextShow") + '</a>');
	} else {
		row.push('');
	}
<#+
	}
}

void WriteTableRowFileScalarItem(DisplaySetItem item, ScalarProperty scalar, Entity entity, string prefix = "")
{
	var aggregatePartPrefix = string.Empty;
	
	if(entity.AggregateRoot != null) {
		 // For file properties declared in aggregate sub-elements, add a entity name prefix in the request URL
		aggregatePartPrefix += entity.Name;
	}

	if(scalar.IsMandatory)
	{
#>
    row.push('<a href="' + appData.siteUrl + 'api/v1/<#= Hyphenate(entity.AggregateName) #>/' + rowData.Id + '/download-<#= Hyphenate(aggregatePartPrefix) + (aggregatePartPrefix != "" ? "-" : "") #><#= Hyphenate(scalar.Name) #>">' + Globalize.formatMessage("TextDownload") + '</a>');
<#+
	}
	else
	{
#>
	if(rowData.<#= scalar.Name #> != null) {
		row.push('<a href="' + appData.siteUrl + 'api/v1/<#= Hyphenate(entity.AggregateName) #>/' + rowData.Id + '/download-<#= Hyphenate(aggregatePartPrefix) + (aggregatePartPrefix != "" ? "-" : "") #><#= Hyphenate(scalar.Name) #>">' + Globalize.formatMessage("TextDownload") + '</a>');
	} else {
		row.push('');
	}
<#+
	}
}
#>
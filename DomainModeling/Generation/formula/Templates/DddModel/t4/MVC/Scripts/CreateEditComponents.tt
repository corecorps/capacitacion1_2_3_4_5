﻿<#+
void WriteCreateEditComponents(string mode)
{
	GetJsCodeGenFileComment();
	
	var view = DddModelRoot.UnirDddModel.MvcModelRoot.GetView(Script.Entity.Name, mode == "Create" ? MvcViewType.Create : MvcViewType.Edit);
	var displaySet = GetFormDisplaySet(view);
#>
function load<#= Script.Entity.Name #><#= mode #>Components() {
<#+
	if(!Script.Entity.AutoGenerateIdentity && "Create".Equals(mode))
	{
		WriteCreateEditComponentsNumericCode("id" + Script.Entity.Name, true, "1", "1000000");
	}

	WriteCreateEditComponentsCombos(displaySet, mode);
	WriteCreateEditComponentsFiles(displaySet);
	WriteCreateEditComponentsBooleans(displaySet);
	WriteCreateEditComponentsNumeric(displaySet);
	WriteCreateEditComponentsTime(displaySet);
	WriteCreateEditComponentsValueObjects(displaySet);
#>

    /**************************************************************************/
    $('#btn-cancel').click(function () {
        appData.goBack();
    });
}
<#+
}

void WriteCreateEditComponentsCombos(DisplaySet displaySet, string mode, string pSaveInstanceRoute = "")
{	
	var saveInstanceRoute = pSaveInstanceRoute;
	
	if(string.IsNullOrEmpty(saveInstanceRoute))
	{
		saveInstanceRoute = "account-saved-parameters/" + Script.Entity.Name + "InstanceParams";
	}

	foreach(var item in displaySet.Items)
	{
		var navigation = Script.Entity.PresentationHierarchicalNavigationProperties
									  .FirstOrDefault(s => !s.IsMultiple && s.RoleName.Equals(item.Expression));
		
		if(navigation == null) continue;
		
		bool isComboBox = DddModelRoot.UnirDddModel.MvcModelRoot.GetSimpleSearchScalarFor(navigation.Entity) != null;
		
		if(isComboBox)
		{		
#>
    $('#cbx-<#= Hyphenate(navigation.RoleName) #>').combobox(defaultApiCombobox({
        url: appData.siteUrl + 'api/v1/<#= Hyphenate(navigation.Entity.AggregateName) #>',
		textOverlabel: Globalize.formatMessage('TextSimpleSearch'),
        toolbar: {
            reset: true<#+ if(item.HasListEnabled()) { #>,<#+ }#>
<#+
	if(item.HasListEnabled())
	{
#> 
            search: function () {
                appData.redirectTo({
                    route: "RedirectToSearchPage/<#= navigation.Entity.Name #>/SearchList/<#= navigation.RoleName #>",
                    saveRoute: '<#= saveInstanceRoute #>',
                    saveParams: get<#= Script.Entity.Name #><#= mode #>Parameters()
                });
            }<#+ if(item.HasCrudEnabled()) { #>,<#+ }#>
<#+
	}
	if(item.HasCrudEnabled())
	{
#> 
            edit: function () {
                appData.redirectTo({
                    route: "RedirectToEditPage/<#= navigation.Entity.Name #>/Edit/" + $('#cbx-<#= Hyphenate(navigation.RoleName) #>').combobox("getId") + "/<#= navigation.RoleName #>",
                    saveRoute: "<#= saveInstanceRoute #>",
                    saveParams: get<#= Script.Entity.Name #><#= mode #>Parameters()
                });
            },
            add: function () {
                appData.redirectTo({
                    route: "RedirectToCreatePage/<#= navigation.Entity.Name #>/Create/<#= navigation.RoleName #>",
                    saveRoute: "<#= saveInstanceRoute #>",
                    saveParams: get<#= Script.Entity.Name #><#= mode #>Parameters()
                });
            }
<#+
	}
#>
        }
    }));
<#+	
		}
		else
		{
		
#>
    $('#cbx-<#= Hyphenate(navigation.RoleName) #>').combobox(DefaultCombobox({
        template: 'search',
		textOverlabel: Globalize.formatMessage('TextSearchNavigation'),
        toolbar: {
            reset: true<#+ if(item.HasListEnabled()) { #>,<#+ }#>
<#+
	if(item.HasListEnabled())
	{
#> 
			search: function () {
                appData.redirectTo({
                    route: "RedirectToSearchPage/<#= navigation.Entity.Name #>/SearchList/<#= navigation.RoleName #>",
                    saveRoute: "<#= saveInstanceRoute #>",
                    saveParams: get<#= Script.Entity.Name #><#= mode #>Parameters()
                });
			}<#+ if(item.HasCrudEnabled()) { #>,<#+ }#>
<#+
	}
	if(item.HasCrudEnabled())
	{
#> 
            edit: function () {
                appData.redirectTo({
                    route: "RedirectToEditPage/<#= navigation.Entity.Name #>/Edit/" + $('#cbx-<#= Hyphenate(navigation.RoleName) #>').combobox("getId") + "/<#= navigation.RoleName #>",
                    saveRoute: "<#= saveInstanceRoute #>",
                    saveParams: get<#= Script.Entity.Name #><#= mode #>Parameters()
                });
            },
            add: function () {
                appData.redirectTo({
                    route: "RedirectToCreatePage/<#= navigation.Entity.Name #>/Create/<#= navigation.RoleName #>",
                    saveRoute: "<#= saveInstanceRoute #>",
                    saveParams: get<#= Script.Entity.Name #><#= mode #>Parameters()
                });
            }
<#+
	}
#>
        }
    }));
<#+	
		}
#>
    setPersistenceStatus($('#cbx-<#= Hyphenate(navigation.RoleName) #>'), "<#= navigation.Entity.SecurityModule #>");
    /**************************************************************************/
<#+
	}
}

void WriteCreateEditComponentsFiles(DisplaySet displaySet)
{
	foreach(var item in displaySet.Items)
	{
		var scalar = Script.Entity.PresentationHierarchicalScalars
								  .FirstOrDefault(s => s.Name.Equals(item.Expression));
		
		if(scalar == null || !scalar.IsFileType) continue;
#>
    fileUpload<#= Script.Entity.Name #><#= scalar.Name #>();
<#+
	}
}

void WriteCreateEditComponentsBooleans(DisplaySet displaySet)
{
	foreach(var item in displaySet.Items)
	{
		var scalar = Script.Entity.PresentationHierarchicalScalars
								  .FirstOrDefault(s => s.Name.Equals(item.Expression));
		
		if(scalar == null || (!"bool".Equals(scalar.Type) && !scalar.IsFileType)) continue;
	
		if(scalar.IsFileType)
		{
			WriteCreateEditComponentsFileCode(scalar.Name);
		}
		else
		{
			WriteCreateEditComponentsBoolCode(scalar.Name);
		}
	}
}

void WriteCreateEditComponentsFileCode(string expr)
{
#>
    /**************************************************************************/
    $('#box-has-<#= Hyphenate(expr) #>').buttonset();
<#+
}

void WriteCreateEditComponentsBoolCode(string expr)
{
#>
    /**************************************************************************/
    $('#box-<#= Hyphenate(expr) #>').buttonset();
<#+
}

void WriteCreateEditComponentsNumeric(DisplaySet displaySet)
{
	foreach(var item in displaySet.Items)
	{
		var scalar = Script.Entity.PresentationHierarchicalScalars
								  .FirstOrDefault(s => s.Name.Equals(item.Expression));
		
		if(scalar == null || !scalar.IsNumericType) continue;
		
		bool isInteger = new [] { "int", "long" }.Contains(scalar.Type);
		string vMin = GetMinValueForNumeric(item, scalar);
		string vMax = GetMaxValueForNumeric(item, scalar);
	
		WriteCreateEditComponentsNumericCode(scalar.Name, isInteger, vMin, vMax);
	}
}

void WriteCreateEditComponentsNumericCode(string expr, bool isInteger, string vMin, string vMax)
{
#>
    $('#txt-<#= Hyphenate(expr) #>')
    .autoNumeric($.extend({}, <#= isInteger ? "appData.autoNumeric.integerFormat" : "appData.autoNumeric.decimalFormat " #>, { vMin: <#= vMin #>, vMax: <#= vMax #> }))
    .spinner({
        min: <#= vMin #>,
        max: <#= vMax #>,
        numberFormat: '<#= isInteger ? "n0" : "n" #>'
    });
<#+
}

void WriteCreateEditComponentsTime(DisplaySet displaySet)
{
	foreach(var item in displaySet.Items)
	{
		var scalar = Script.Entity.PresentationHierarchicalScalars
								  .FirstOrDefault(s => s.Name.Equals(item.Expression));
		
		if(scalar == null || !"time".Equals(scalar.Type)) continue;
		
		WriteCreateEditComponentsTimeCode(scalar.Name);
	}
}

void WriteCreateEditComponentsTimeCode(string expr)
{
#>
	$('#txt-<#= Hyphenate(expr) #>').timepicker();
<#+
}

void WriteCreateEditComponentsValueObjects(DisplaySet displaySet)
{
	foreach(var item in displaySet.Items)
	{
		var navigation = Script.Entity.PresentationHierarchicalNavigationProperties
									  .Where(np => np.Entity is ValueObject)
									  .FirstOrDefault(np => np.RoleName.Equals(item.Expression));
																		
		if(navigation == null) continue;
		
		foreach(var scalar in navigation.Entity.Scalars)
		{
			switch(scalar.Type)
			{
				case "int":
				case "long":
					WriteCreateEditComponentsNumericCode(
						navigation.RoleName + scalar.Name, 
						true, 
						GetMinValueForNumeric(item, scalar), 
						GetMaxValueForNumeric(item, scalar));
					break;
				case "decimal":
				case "currency":
				case "double":
				case "percentage":
					WriteCreateEditComponentsNumericCode(
						navigation.RoleName + scalar.Name, 
						false, 
						GetMinValueForNumeric(item, scalar), 
						GetMaxValueForNumeric(item, scalar));
					break;
				case "time":
					WriteCreateEditComponentsTimeCode(navigation.RoleName + scalar.Name);
					break;
				case "date":
				case "datetime":
					break;
				case "audio":
				case "video":
				case "file":
				case "image":
					WriteCreateEditComponentsFileCode(navigation.RoleName + scalar.Name);
					break;
				case "bool":
					WriteCreateEditComponentsBoolCode(navigation.RoleName + scalar.Name);
					break;
				case "char":
				case "binary":
					break;
				case "email":
				case "url":
				case "string":
					break;
				default:
					break;
			}
		}
	}
}
#>